name := "web-lab4"

version := "0.1"

scalaVersion := "2.13.1"

lazy val mainDependencies: Seq[ModuleID] = Seq(
  // Все приложения Spring Boot конфигурируются от spring-boot-starter-parent
  "org.springframework.boot" % "spring-boot-starter-parent" % "2.2.2.RELEASE",

  //неявно определяет все остальные зависимости, такие как spring-core, spring-web, spring-webmvc, servlet api, и библиотеку jackson-databind
  "org.springframework.boot" % "spring-boot-starter-web" % "2.2.2.RELEASE",

  "org.springframework.boot" % "spring-boot-starter-data-jpa" % "2.2.2.RELEASE",

  "org.springframework.boot" % "spring-boot-starter-actuator" % "2.2.2.RELEASE",

  "org.postgresql" % "postgresql" % "42.2.9",

  "org.springframework.boot" % "spring-boot-starter-security" % "2.2.2.RELEASE",

  "org.springframework.boot" % "spring-boot-starter-actuator" % "2.2.2.RELEASE",
)

libraryDependencies ++= mainDependencies



// set the main entrypoint to the application that is used in startup scripts
mainClass := Some("app.Application")