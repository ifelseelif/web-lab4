package app.service

import app.db.{PointRepository, UserRepository}
import app.pojo._
import javax.transaction.Transactional
import org.springframework.stereotype.Service

import scala.jdk.CollectionConverters._
import scala.util.Try

@Service
class PointDTOService(val pointRepository: PointRepository, val userRepository: UserRepository) {

  @Transactional
  def savePoint(point: PointDTO, user: String): Try[Unit] = Try(
    pointRepository save Point.fromPointDTO(userRepository.findByUsername(user))(point)
  )

  @Transactional
  def deletePointsByUser(user: String): Try[Unit] =
    Try(pointRepository.deleteAllByOwner(userRepository.findByUsername(user)))

  def getAllPointsByUser(user: String): Try[List[PointDTO]] =
    Try(
      pointRepository.findAllByOwner(userRepository.findByUsername(user)).asScala.map(point => PointDTO.fromPoint(point)).toList
    )
}
