package app.service

import app.db.UserRepository
import app.pojo.{User, UserDTO}
import javax.transaction.Transactional
import org.springframework.security.core.userdetails.{UserDetails, UserDetailsService}
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

import scala.util.Try

@Service
class UserDTOService(val userRepository: UserRepository, val passwordEncoder: PasswordEncoder) extends UserDetailsService{

  @Transactional
  def register(userDTO: UserDTO): Try[User] =
    Try(userRepository save User.fromUserDTO(passwordEncoder)(userDTO))

  def isUsernameFree(username: String): Try[Boolean] =
    Try(userRepository countByUsername username) map {case 0 => true; case _ => false}

  override def loadUserByUsername(username: String): UserDetails =
    UserDTO.fromUser(userRepository.findByUsername(username))

}
