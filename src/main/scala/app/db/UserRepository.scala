package app.db

import app.pojo.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
trait UserRepository extends JpaRepository[User, Long]{
  def countByUsername(username: String): Int
  def findByUsername(username: String): User
}
