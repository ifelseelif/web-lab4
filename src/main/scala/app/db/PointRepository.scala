package app.db

import java.util
import app.pojo.{Point, User}
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
trait PointRepository extends JpaRepository[Point, Long] {
  def findAllByOwner(user: User): util.List[Point]
  def deleteAllByOwner(user : User)
}