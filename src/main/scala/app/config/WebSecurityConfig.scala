package app.config

import app.service.UserDTOService
import javax.sql.DataSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.configuration.{EnableWebSecurity, WebSecurityConfigurerAdapter}
import org.springframework.context.annotation.Bean
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCrypt
import org.springframework.security.crypto.password.PasswordEncoder

@Configuration
@EnableWebSecurity
class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  @Autowired
  var dataSource: DataSource = _

  @Autowired
  var userDTOService: UserDTOService = _

  @throws[Exception]
  override protected def configure(http: HttpSecurity): Unit =
    http.csrf.disable
      .authorizeRequests
        .antMatchers("/css/*", "/js/*", "/error", "/reg", "/registration")
        .permitAll
        .anyRequest
        .authenticated
        .and
      .formLogin
        .loginPage("/login")
        .permitAll
        .and
      .logout
        .permitAll

  @Bean
  def customPasswordEncoder: PasswordEncoder = new PasswordEncoder() {
    override def encode(rawPassword: CharSequence): String = BCrypt.hashpw(rawPassword.toString, BCrypt.gensalt(4))

    override def matches(rawPassword: CharSequence, encodedPassword: String): Boolean = BCrypt.checkpw(rawPassword.toString, encodedPassword)
  }


  @throws[Exception]
  override protected def configure(auth: AuthenticationManagerBuilder): Unit = {
    auth.userDetailsService(userDTOService).passwordEncoder(customPasswordEncoder)
  }
}
