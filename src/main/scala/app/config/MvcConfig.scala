package app.config

import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@Configuration class MvcConfig extends WebMvcConfigurer {
  override def addViewControllers(registry: ViewControllerRegistry): Unit = {
    registry.addViewController("/login").setViewName("login.html")
    registry.addViewController("/").setViewName("index.html")
    registry.addViewController("/reg").setViewName("registration.html")
  }
}