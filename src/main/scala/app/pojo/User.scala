package app.pojo

import javax.persistence._
import java.util

import org.springframework.security.crypto.password.PasswordEncoder

import scala.jdk.CollectionConverters._

@Entity
@Table(name = "USERS")
case class User(
 var username: String,
 var password: String,
 var points: List[Point]
) {

  def this() = this(null, null, null)

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  var userId: Long = _

  @OneToMany
  var pointsCol: util.List[Point] = points.asJava

  def getUserId: Long = userId
  def getUsername: String = username
  def getPassword: String = password
  def getPoints: util.List[Point] = points.asJava

  def setUserId(userId: Long): Unit = this.userId = userId
  def setUsername(username: String): Unit = this.username = username
  def setPassword(password: String): Unit = this.password = password
  def setPoints(points: util.List[Point]): Unit = this.points = points.asScala.toList
}

object User{
  def apply(username: String, password: String): User =
    User(username, password, null)

  def fromUserDTO: UserDTO => User = {
    case UserDTO(username, password) => User(username, password)
  }

  def fromUserDTO(passwordEncoder: PasswordEncoder): UserDTO => User = {
    case UserDTO(username, password) => User(username, passwordEncoder.encode(password))
  }
}