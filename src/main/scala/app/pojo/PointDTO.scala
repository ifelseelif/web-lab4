package app.pojo

import javax.validation.constraints._

case class PointDTO(
  @NotNull(message = "X should not be empty") @Max(value = 2, message = "X is not bigger than 2") @Min(value = -2, message = "X is not smaller than -2") var x: Double,
  @NotNull(message = "Y should not be empty") @Max(value = 5, message = "X is not bigger than 5") @Min(value = -3, message = "Y is not smaller than -3") var y: Double,
  @NotNull(message = "R should not be empty") @Max(value = 2, message = "R is not bigger than 2") @Min(value = -2, message = "R is not smaller than -2") var r: Double,
  var result: Boolean
){
  def getX: Double       = x
  def getY: Double       = y
  def getR: Double       = r
  def getResult: Boolean = result

  def setX(x: Double): Unit            = this.x = x
  def setY(y: Double): Unit            = this.y = y
  def setR(r: Double): Unit            = this.r = r
  def setResult(result: Boolean): Unit = this.result = result
}

object PointDTO{
  def apply(x: Double, y: Double, r: Double): PointDTO = // God pls
    this(x, y, r, PointDTO.checkArea(x, y, r))

  def checkArea(point: PointDTO): Boolean = point match {
    case PointDTO(x, y, r, _) => checkArea(x, y, r)
  }

  def checkArea(x: Double, y: Double, r: Double): Boolean =
    List.apply[(Double, Double, Double) => Boolean](
      { case (x, y, r) => (x <= 0 && y <= 0) && (x * x + y * y <= r * r) }, // left-bottom circle
      { case (x, y, r) => (y >= 0 && x <= 0) && (x >= -r/2 && y <= r) },    // left-top rectangle
      { case (x, y, r) => (y >= 0 && x >= 0) && (y <= r - x) }              // right-top triangle
    ).exists(validator => validator(x, y, r))

  def fromPoint: Point => PointDTO = {
    case Point(x, y, r, result,_) => PointDTO(x, y, r, result)
  }
}