package app.pojo

import java.util

import com.sun.istack.NotNull
import javax.validation.constraints.NotEmpty
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

case class UserDTO(
  @NotEmpty @NotNull var username: String,
  @NotEmpty @NotNull var password: String
) extends UserDetails {

  def this() = this(null, null)

  def setUsername(username: String): Unit = this.username = username

  def setPassword(password: String): Unit = this.password = password

  override def getAuthorities: util.Collection[_ <: GrantedAuthority] = null

  override def getPassword: String = password

  override def getUsername: String = username

  override def isAccountNonExpired: Boolean = true

  override def isAccountNonLocked: Boolean = true

  override def isCredentialsNonExpired: Boolean = true

  override def isEnabled: Boolean = true
}

object UserDTO{

  def fromUser: User => UserDTO = {
    case User(username, password, _) => new UserDTO(username, password)
  }
}
// DB Postgre <-JPA-> Point <-HandMade converter-> PointDTO <-Spring, Vue-> Web Client