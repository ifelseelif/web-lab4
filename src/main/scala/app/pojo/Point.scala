package app.pojo

import javax.persistence._

@Entity
@Table(name = "POINTS") // TODO use not a user but a userId
case class Point(var x: Double, var y: Double, var r: Double, var result: Boolean, @ManyToOne @JoinTable(name = "user_user_id") var owner: User) {

  def this() = this(0,0,0,false,null)

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  var id: Long = _

  def getId: Long = id
  def getX : Double = this.x
  def getY : Double = this.y
  def getR : Double = this.r
  def getResult : Boolean = this.result
  def getOwner: User = owner

  def setId(id: Long): Unit = this.id = id
  def setOwner(user: User): Unit = this.owner = user
  def setX(x : Double) : Unit = this.x = x
  def setY(y : Double) : Unit = this.y = y
  def setR(r : Double) : Unit = this.r = r
  def setResult(result : Boolean) : Unit = this.result = result
}

object Point {

  def fromPointDTO(user: User): PointDTO => Point = {
    case PointDTO(x, y, r, result) => Point(x, y, r, result, user)
  }
}
