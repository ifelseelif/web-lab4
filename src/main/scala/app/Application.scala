package app

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.transaction.annotation.EnableTransactionManagement



object Application extends App {
  SpringApplication.run(classOf[Application], args :_ *)
}

@SpringBootApplication
@EnableJpaRepositories
@EnableTransactionManagement
class Application {}