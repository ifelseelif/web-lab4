package app.controllers

import java.security.Principal
import java.util

import app.pojo._
import app.service.PointDTOService
import javax.servlet.http.HttpServletResponse
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation._

import scala.jdk.CollectionConverters._
import scala.util.{Failure, Success}

// Option[Int] : Some[Int] | None
// Either[String, Int] : Right[Int] | Left[String]
// Try[Int] <- Either[Throwable, Int] : Success[Int] | Failure (Failure(ex: Throwable))

@RestController
class PointController(val pointDTOService: PointDTOService) {

  @PostMapping(value = Array("/points"))
  @ResponseBody
  def addPoint(@RequestBody point: PointDTO, bindingResult: BindingResult, principal: Principal, httpServletResponse: HttpServletResponse): String =
    bindingResult.getAllErrors.asScala.toList
      .map(_.getDefaultMessage) match {
      case Nil =>
        pointDTOService.savePoint(point.copy(result = PointDTO.checkArea(point)), principal.getName)
        httpServletResponse.setStatus(200)
        Success("OK").toString
      case list if list.nonEmpty =>
        httpServletResponse.setStatus(500)
        Failure(new RuntimeException(list.mkString("\n"))).toString
    }

  @GetMapping(value = Array("/points"))
  @ResponseBody
  def showAllPoints(principal: Principal, httpServletResponse: HttpServletResponse): util.List[PointDTO] =
    pointDTOService
      .getAllPointsByUser(principal.getName)
      .getOrElse {
        httpServletResponse.setStatus(500)
        List.empty[PointDTO]
      }.asJava

  @DeleteMapping(value = Array("/clear"))
  @ResponseBody
  def clearPoints(principal: Principal, httpServletResponse: HttpServletResponse): String =
    pointDTOService.deletePointsByUser(principal.getName).fold(
      failure => {
        httpServletResponse.setStatus(500)
        failure.toString
      },
      _ =>Success("OK").toString
    )
}
