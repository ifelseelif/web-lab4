package app.controllers

import org.springframework.boot.web.servlet.error.ErrorController
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping


@Controller
class VueErrorController extends ErrorController {
  override def getErrorPath: String = "/error"

  @RequestMapping(Array("/error"))
  def handleError: String = {
    "error.html"
  }
}
