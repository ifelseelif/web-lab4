package app.controllers

import app.pojo.UserDTO
import app.service.UserDTOService
import javax.servlet.http.{HttpServletRequest, HttpServletResponse}
import org.springframework.web.bind.annotation.{RequestMapping, RequestMethod, ResponseStatus, RestController}

@RestController
class UserController(val userDTOService: UserDTOService) {

  @RequestMapping(path = Array("/registration"), method = Array(RequestMethod.POST))
  @throws[Exception]
  def addUser(user: UserDTO, request: HttpServletRequest, response: HttpServletResponse): Unit = {
    if (!userDTOService.isUsernameFree(user.getUsername).getOrElse(false)) response.sendRedirect("/reg?error")
    else {
      userDTOService.register(user)
        .foreach(_ => request.login(user.getUsername, user.getPassword))
      response.sendRedirect("/")
    }
  }
}


