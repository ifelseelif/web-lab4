let canvas = document.getElementById(graphName);
canvas.addEventListener("mousedown", function (event) {
    let click_x;
    let click_y;
    if (!isNaN(globalRadius)) {
        let rect = canvas.getBoundingClientRect();
        click_x = event.clientX - rect.right + rect.width / 2;
        click_y = rect.bottom - event.clientY - rect.height / 2;
        click_x = (click_x / 120) * globalRadius;
        click_y = (click_y / 120) * globalRadius;
        pushBack(click_x,click_y,globalRadius);
    }
});

function pushBack(x, y, r) {
    sendRequest([{name: "graph", value: `${x}:${y}:${r}`}]);
    drawFigure(globalRadius,document)
}

